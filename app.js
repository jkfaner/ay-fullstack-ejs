const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.get("/", function(res, req) {
    
    let day = new Date();

    if (day.getDate() !== 6 || day.getDate() !== 0) {
        req.sendfile(__dirname + "/index.html");
    }
});

app.listen(3000, function() {
    console.log("Server is running on port 3000...");
});